import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit";
import { v4 } from "uuid";
import { fetchTodos } from "../../api";
import type { RootState } from "../../store";
import { Todo } from "../../type";

// Define a type for the slice state
interface TodosState {
  todos: Todo[];
}

// Define the initial state using that type
const initialState: TodosState = {
  todos: [] as Todo[],
};

export const fetchAllTodos = createAsyncThunk(
  "todos/fetchAllTodos",
  async () => {
    const response = await fetchTodos();
    return response as Todo[];
  }
);

export const todosSlice = createSlice({
  name: "todos",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,
  reducers: {
    updateTodo: (state, action: PayloadAction<Todo>) => {
      state.todos = [
        ...state.todos,
        {
          id: v4(),
          title: action.payload.title,
          desc: action.payload.desc,
          isComplete: action.payload.isComplete,
        },
      ];
    },
    deleteTodo: (state, action: PayloadAction<string>) => {
      state.todos = state.todos.filter(({ id }) => id !== action.payload);
    },
    // Use the PayloadAction type to declare the contents of `action.payload`
  },
  extraReducers: {
    [fetchAllTodos.fulfilled]: (state, action) => {
      return { ...state, todos: action.payload };
    },
  },
});

export const { updateTodo, deleteTodo } = todosSlice.actions;

// Other code such as selectors can use the imported `RootState` type
export const selectTodos = (state: RootState) => state.todos.todos;

export default todosSlice.reducer;
